﻿using UnityEngine;

public class DetectOrientation : MonoBehaviour {
    
    /// Event and delegate to handle device orientation
    /// changing recognition.
    public delegate void ScreenOrientationChangedHandler();
    public static event ScreenOrientationChangedHandler DeviceOrientationChangeEvent;

    /// Current device orientation
    private static DeviceOrientation currentDeviceOrientation = DeviceOrientation.Unknown;
    
    /// Check if current device orientation is 
    /// PORTRAIT
    public static bool IsPortarait
    {
        get {
            return Input.deviceOrientation == DeviceOrientation.Portrait ||
                   Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown; 
        }
    }

    /// Check if current device orientation is 
    /// LANDSCAPE
    public static bool IsLandscape
    {
        get
        {
            return Input.deviceOrientation == DeviceOrientation.LandscapeLeft ||
                   Input.deviceOrientation == DeviceOrientation.LandscapeRight;

        }
    }

    /// Current Device Orientation is accessible from others
    /// only from this property. New setting will trigger
    /// an event
    public static DeviceOrientation CurrentScreenOrientation
    {
        get
        {
            return currentDeviceOrientation;
        }
        set
        {
            if (currentDeviceOrientation == value)
                return;

            currentDeviceOrientation = value;
            ScreenOrientationChanged();
        }
    }
    
    void Start () {
        // Calibration
        CurrentScreenOrientation = Input.deviceOrientation;   	
	}
	
	void Update () {
		if(Input.deviceOrientation != CurrentScreenOrientation)
        {
            CurrentScreenOrientation = Input.deviceOrientation;
        }
	}

    public static void ScreenOrientationChanged()
    {
        if (DeviceOrientationChangeEvent != null)
        {
            DeviceOrientationChangeEvent();
        }
    }
}
