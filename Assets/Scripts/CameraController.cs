﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float smooth = 5.0f;

    Vector3 destination;
    Vector3 start;
    float i = 0;

    private void Awake()
    {
        destination = new Vector3(0, -0.75f, -10.0f);
    }

    private void OnEnable()
    {
        start = transform.position;
        DetectOrientation.DeviceOrientationChangeEvent += OnScreenChange;
    }

    private void OnDisable()
    {
        DetectOrientation.DeviceOrientationChangeEvent -= OnScreenChange;
    }

    private void FixedUpdate()
    {
        i = Mathf.Clamp(i + Time.deltaTime * smooth, 0, 1);
        transform.position = Vector3.Lerp(start, destination, i);
    }

    private void OnScreenChange()
    {
        start = transform.position;

        if (DetectOrientation.IsLandscape)
        {
            destination = new Vector3(1.5f, 1f, -10.0f);
            Camera.main.orthographicSize = 1.5f;
        }
        else
        {
            destination = new Vector3(0, -0.75f, -10.0f);
            Camera.main.orthographicSize = 2.85f;
        }
        
        
        i = 0;
    }
}
